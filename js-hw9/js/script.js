"use strict";
// ПРОСТА СКЛАДНІСТЬ

// function createList(array, parent = document.body) {
//     const ol = document.createElement('ol');
//     parent.prepend(ol);

//     for(let i = 0; i < array.length; i++) {
//         let li = document.createElement('li');
//         ol.append(li);
//         let elem = array[i];
//         li.innerHTML = elem;
//       } 
      
// }

// ПІДВИЩЕНА СКЛАДНІСТЬ

function createList(array, parent = document.body) {
    const ol = document.createElement('ol');
    parent.prepend(ol);

    let list = array.map(function createList2 (arrElements){
        let li = document.createElement('li');
        ol.append(li);
        let elem = arrElements;

        if(Array.isArray(arrElements)) {
            createList(arrElements, li);

        } else {
            li.innerHTML = elem;
        }       
    });
}

createList(["Kharkiv", "Kyiv", ["Boryspil", "Irpin", ["Rivne", "Poltava", ["Lutsk", "Vinnytsya"]]], "Odesa", "Lviv", "Dnipro"]);
