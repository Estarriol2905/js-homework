"use strict";
function changeSlide () {
    let banners = document.querySelectorAll(".image-to-show");
    let wrapper = document.querySelector('.images-wrapper');
    let timer = setInterval(setClass, 3000);
    let i = 1;
    let j = 0;

    function setClass() {
        banners[j].classList.remove('active');
        banners[i].classList.add('active');
        i++;
        j++;
        if (i === banners.length){
            i = 0;
        }
        if (j === banners.length){
            j = 0;
        }
    };

    function createButtons() {
        let wrap = document.createElement('div');
        let stopShow = document.createElement('button');
        let startShow = document.createElement('button');

        stopShow.textContent = "Припинити";
        startShow.textContent = "Відновити показ";
        wrap.className = "wrap-buttons";
        stopShow.className = "button-stop";
        startShow.className = "button-start";
        wrapper.after(wrap);
        wrap.prepend(stopShow);
        wrap.append(startShow);

        stopShow.addEventListener("click", () => {
            clearInterval(timer);
        });
        startShow.addEventListener("click", () => {
            clearInterval(timer);
            timer = setInterval(setClass, 3000);
        });
    };

setTimeout(createButtons, 3500);
};

changeSlide ();





