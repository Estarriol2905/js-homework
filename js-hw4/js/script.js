"use strict";
    let num1;
    let num2;
    let oper;
    //приймає дані від користувача
    function enterNumbers() {
        num1 = prompt("Введіть число 1", num1);
        num2 = prompt("Введіть число 2", num2);
        oper = prompt("Введіть один з цих операторів: +, -, *, /", oper);
        if (num1 === null || num2 === null || oper === null) {
            return false;
        }
        checkNumbers();
    }
    //первіряє правильність внесених даних
    function checkNumbers() {
        if (isNaN(num1) || num1.trim() === "" || isNaN(num2) || num2.trim() === "") {
            alert("Потрібно ввести число");
            enterNumbers();
        } else if (oper != "+" && oper != "-" && oper != "*" && oper != "/"){
            alert("Потрібно ввести один з цих операторів: +, -, *, /");
            enterNumbers();
        }
        else {
            sumNumbers();
        }
    }
    // виконує необхідні операції
    function sumNumbers(){
        num1 = Number(num1);
        num2 = Number(num2);
        if (oper === "+") {
            return num1 + num2;
        } else if (oper === "-"){
            return num1 - num2;
        } else if (oper === "*"){
            return num1 * num2;
        } else if (oper === "/"){
            return num1 / num2;
        }
    }

    enterNumbers();
    console.log(sumNumbers());