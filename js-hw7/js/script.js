"use strict";
let array = ['hello', 'world', 23, '23', null, false, {}, 45, true, NaN, {}, 33];

function filterBy(arr, dataType) {
    let newArr = [];

    for (let elem of arr) {
        let elemType = typeof(elem);
            if (elem === null){
                elemType = "null";
            }
            if (dataType === elemType) {
                continue;
            };
        newArr = newArr.concat(elem);
      }
    return newArr;
}
 
console.log(array);
console.log(filterBy(array, "string"));
