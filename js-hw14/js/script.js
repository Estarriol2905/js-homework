"use strict";
    let body = document.querySelector('body');
    let button = document.querySelectorAll(".button");
    let footerButton = document.querySelector(".footer-button");
    let toggle = 'dark';
    let localTheme = localStorage.getItem('theme');
    
    if (localTheme === 'themeLight') {
        addClass();
    } else {
        deleteClass();
    }

    function deleteClass() {
        for (let i = 0; i < button.length; i++) {
            button[i].classList.remove('button-light');
        }
        body.classList.remove('body-light');
        toggle = 'dark';
    };

    function addClass() {
        for (let i = 0; i < button.length; i++) {
            button[i].classList.add('button-light');
        }
        body.classList.add('body-light');
        toggle = 'light';
    };

    footerButton.addEventListener("click", () => {
        if (toggle === 'light') {
            deleteClass();
            localStorage.setItem('theme', 'themeDark');
        } else {
            addClass();
            localStorage.setItem('theme', 'themeLight');
        }
    });
    

    






