"use strict";
const showPass = document.getElementById("show-password");
const inputPass = document.getElementById("input-password");
const iconPass = document.getElementById("icon-password");

const showCheckPass = document.getElementById("show-check-password");
const inputCheckPass = document.getElementById("input-check-password");
const iconCheckPass = document.getElementById("icon-check-password");

const mainForm = document.forms.main;
const inputPassName = mainForm.enterPass;
const inputCheckPassName = mainForm.checkPass;
let error;

function showHidePass (input, icon) {
    if(input.getAttribute('type') == 'password') {
        input.removeAttribute('type');
        input.setAttribute('type', 'text');
        icon.className = 'fas fa-eye-slash icon-password';
    } else {
        input.removeAttribute('type');
        input.setAttribute('type', 'password');
        icon.className = 'fas fa-eye icon-password';
    }
}

showPass.addEventListener('click', function (){
    showHidePass(inputPass, iconPass);
});

showCheckPass.addEventListener('click', function (){
    showHidePass(inputCheckPass, iconCheckPass);
});

mainForm.addEventListener('submit', function (event) {
    let inputPassValue = inputPassName.value;
    let inputCheckPassValue = inputCheckPassName.value;
    
    if(inputPassValue !== inputCheckPassValue){
        if (error === undefined){
            error = inputCheckPassName.parentElement.insertAdjacentHTML(
                'beforeend', '<div class="main-form-error">Потрібно ввести однакові значення</div>'
            );
            error = document.getElementsByClassName('main-form-error');
        }
        event.preventDefault();
    }
    else {
        alert("You are welcome");
    }
});