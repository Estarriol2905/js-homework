"use strict";
let btn = document.getElementsByClassName("btn");

document.addEventListener('keydown', function(event) {
    
    for (let i = 0; i < btn.length; i++) {
        if (event.code === btn[i].innerHTML || event.code === "Key" + btn[i].innerHTML) {
            btn[i].classList = 'btn active';
        }
        else {
            btn[i].classList = 'btn';
        };
    }
});