"use strict";
let userName;
let userLastName;
let userAge;
let userNameNumber; // змінна створена для перевірки поля userName, що там не число.

do {
    userName = prompt("What is your name?", userName);
    userNameNumber = Number(userName);
} while (userNameNumber == userName || userName === "");

do {
   userAge = prompt("How old are you?", userAge);
} while (isNaN(userAge) || userAge === "");

if (userAge < 18){
    alert("You are not allowed to visit this website");
} else if (userAge >=18 && userAge <=22){
    let sure = confirm("Are you sure you want to continue?");
    if (sure === true){
        alert("Welcome, " + userName);
    } else{
        alert("You are not allowed to visit this website");
    }
} else {
    alert("Welcome, " + userName);
}