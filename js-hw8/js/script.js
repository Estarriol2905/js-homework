"use strict";
let par = document.getElementsByTagName("p");

for (let i = 0; i < par.length; i++) {
    par[i].style.backgroundColor = "#ff0000";  
};

let optionsList = document.querySelector('#optionsList');
console.log(optionsList);

let parent = optionsList.parentElement;
console.log(parent);

let nodes = optionsList.childNodes;

for (let i = 0; i < nodes.length; i++) {
    let nodeName = nodes[i].nodeName;
    let nodeType = nodes[i].nodeType;
    console.log(nodeName + " : " + nodeType);
}

let testParagraph = document.querySelector('#testParagraph');
testParagraph.innerHTML = "This is a paragraph";

let mainHeader = document.querySelector('.main-header');
for (let elem of mainHeader.children) {
    elem.classList.add('nav-item');
   console.log(elem);
  };

let optionsListTitle = document.querySelectorAll('.options-list-title');
    console.log(optionsListTitle);
for (let elem of optionsListTitle) {
    elem.classList.remove('options-list-title');
  };

