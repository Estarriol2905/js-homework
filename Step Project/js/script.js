"use strict";
function addTab (active1, active2) {
    let tabNav = document.querySelectorAll('.services-tab-title'),
        tabContent = document.querySelectorAll('.services-info'),
        tabName;

    tabNav.forEach(item => {
        item.addEventListener('click', selectTabNav)
    });

    function selectTabNav() {
        tabNav.forEach(item => {
            item.classList.remove(active1);
        });
        this.classList.add(active1);
        tabName = this.getAttribute('data-tab-name');
        selectTabContent(tabName);
    }

    function selectTabContent(tabName) {
        tabContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add(active2) : item.classList.remove(active2);
        })
    }
};

addTab('active-title', 'active-services');

function sortCards (active1) {
    let list = document.querySelectorAll('.our-work-list');
    let card = document.querySelectorAll('.our-works-card');
    let tabName;
    let ourWorks = document.querySelector(".our-works");
    let loadMoreBtn = document.querySelector(".load-more-btn");
    let height = '645px';

    list.forEach(item => {
        item.addEventListener('click', selectTabNav)
    });

    function selectTabNav() {
        list.forEach(item => {
            item.classList.remove(active1);
        });
        this.classList.add(active1);
        tabName = this.getAttribute('data-tab-name');
        if(tabName === "all"){
            tabName = "our-works-card";
        }
        selectTabContent(tabName);
    }

    loadMoreBtn.addEventListener("click", () => {
        ourWorks.style.overflow = "visible";
        if(tabName === "our-works-card"){
            ourWorks.style.height = "1290px";
        }else {
            ourWorks.style.height = "100%";
        }
        height = "1290px";
        loadMoreBtn.remove();
    });

    function selectTabContent(tabName) {
        card.forEach(item => {
           if (!item.classList.contains(tabName)){
                item.classList.remove('visible');
            }
            else {
                item.classList.add('visible');
            }
        })
        if(tabName === "our-works-card"){
            ourWorks.style.height = height;
        }else {
            ourWorks.style.height = "100%";
        }
    }

};

sortCards('active-tab-work');

new Swiper('.review-slider', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },
    loop: true,
    thumbs: {
        swiper: {
            el: '.arrow-slider',
            slidesPerView: 2,
        },
        swiper: {
            el: '.mini-slider',
            slidesPerView: 4,
        },
    },
});