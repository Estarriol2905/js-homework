"use strict";

function createNewUser() {
    const newUser = {
        firstName: prompt("Введіть своє ім'я"),
        lastName: prompt("Введіть своє прізвище"),
        birthday: prompt("Введіть свою дату народження у форматі: dd.mm.yyyy", "01.01.1990"),
        getLogin: function (){
        return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge: function () {
            let day = this.birthday.slice(0,2);
            let month = this.birthday.slice(3,5);
            let year = this.birthday.slice(6,10);
            let today = new Date();
            let birthDate = new Date(year, month - 1, day);
            let age = today.getFullYear() - birthDate.getFullYear();
            let difMonth = today.getMonth() - birthDate.getMonth();
                if (difMonth < 0 || (difMonth === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }
            return age;
        },
        getPassword: function (){
            let year = this.birthday.slice(6,10);
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + year;
        },
    };
    return newUser;
}

const user = createNewUser();

console.log(user);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
